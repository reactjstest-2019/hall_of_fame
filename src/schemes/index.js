import Joi from '@hapi/joi';

export const userLoginSchema = Joi.object().keys({
    email: Joi.string().email({ tlds: { allow: ['com', 'net'] } }).required(),
    password: Joi.string().min(4).required()
})
