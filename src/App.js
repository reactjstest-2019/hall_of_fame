import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import * as ROUTES from './constants/routes';
import { Header, Error404, PrivateRoute, ErrorBoundary } from './components';
import { Home, Login, Actors, ActorDetails } from './screens';
import { isAuthenticated } from './util/index';

const App = props =>
  <ErrorBoundary>
    <BrowserRouter>
      <Header />
      <Switch>
        {/* normal routes*/}
        <Route path={ROUTES.HOME} exact component={Home} />
        <Route path={ROUTES.LOGIN} component={Login} />
        {/* protected routes*/}
        <PrivateRoute
          path={ROUTES.ACTORS_LIST_VIEW}
          isAuthenticated={isAuthenticated(props.user)}
          exact
          component={Actors}
        />
        <PrivateRoute
          path={ROUTES.ACTORS_DETAIL_VIEW}
          component={ActorDetails}
          isAuthenticated={isAuthenticated(props.user)}
        />
        <Route component={Error404} />
      </Switch>
    </BrowserRouter>
  </ErrorBoundary>

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(App);
