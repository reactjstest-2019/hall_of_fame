export const HOME = '/'
export const LOGIN = '/login'
export const ACTORS_LIST_VIEW = '/actors'
export const ACTORS_DETAIL_VIEW = '/actors/:id'
