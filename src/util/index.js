import { USER } from '../constants/localStorageKeys';

export const isAuthenticated = user => {
    return (user && user.email) || localStorage.getItem(USER) ? true : false
}
