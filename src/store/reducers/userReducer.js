import * as actionTypes from '../actions';

const initialState = {
    email: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USER_LOGIN:
            return {
                ...state,
                email: action.data
            };
        case actionTypes.USER_LOGOUT:
            return {
                ...state,
                email: ''
            }
        default:
            return state;
    }
};

export default reducer;
