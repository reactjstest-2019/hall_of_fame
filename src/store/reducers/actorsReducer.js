import * as actionTypes from '../actions';

const initialState = {
    actors: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ACTORS_UPDATE:
            const copy = Object.assign({}, state)
            copy.actors[`p${action.data.page}`] = action.data.list
            copy.totalCount = action.data.totalCount
            copy.currentPage = action.data.page
            return copy
        default:
            return state;
    }
};

export default reducer;
