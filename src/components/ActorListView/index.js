import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Intent } from '@blueprintjs/core';
import './index.scss';
const ActorListView = props =>
    <>
        {props.list ? props.list.map(item => {
            return (
                <div className="actorContainer" key={item.id}>
                    <h3>Actor: {item.first_name} {item.last_name}</h3>
                    <p>{item.bio}</p>
                    <br />
                    <p className="detailBtn">
                        <Button large intent={Intent.SUCCESS} color="#fff"><Link to={`/actors/${item.id}`}>View Details ></Link></Button>
                    </p>
                </div>
            )
        }) : <h1>No actors yet...</h1>
        }
    </>

export default ActorListView;