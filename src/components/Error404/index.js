import React from 'react';
import * as styles from './index.module.scss';

const Error404 = () =>
    <>
        <div className={styles.container}>
            <h1>Not found! I'm so sorry :(</h1>
        </div>
    </>

export default Error404;