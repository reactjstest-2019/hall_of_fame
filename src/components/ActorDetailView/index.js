import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom';
import axios from 'axios'
import './index.scss';

const ActorDetailView = props => {
    const [data, setData] = useState()
    try {
        useEffect(() => {

            const fetchData = async () => {
                const result = await axios(
                    `${process.env.REACT_APP_API_SERVER}/actors/${props.match.params.id}`,
                );
                setData(result.data);
            };
            fetchData()

        }, [props.match.params.id])
    } catch (err) {
        console.log(err)
    }

    return (
        <>
            {data ?
                <div className="detailContainer">
                    <img src={data.data.actor.img_url} alt="actor model" />
                    <h3>First Name: {data.data.actor.first_name}</h3>
                    <h3>Last Name: {data.data.actor.last_name}</h3>
                    <p>Bio: <br /> {data.data.actor.bio}</p>
                    <p>Age: {data.data.actor.age}</p>
                    Featured Movies:
                        <ul>
                        {data.data.actor.featured_movies.map((movie, i) => {
                            return (
                                <li key={i}>{movie}</li>
                            )
                        })}
                    </ul>
                </div>
                : <h1>Actor not found</h1>}
        </>
    )
}

export default withRouter(ActorDetailView);
