import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { LOGIN } from '../../constants/routes';

const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => (
    <Route {...rest} render={(props) => (
        isAuthenticated === true
            ? <Component {...props} />
            : <Redirect to={LOGIN} />
    )} />
)

export default PrivateRoute;
