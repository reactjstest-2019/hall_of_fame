import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Checkbox, Intent } from '@blueprintjs/core';
import './index.scss';
import { userLoginSchema } from '../../schemes/index';
import { validUser } from '../../seeds/index';
import * as actionTypes from '../../store/actions';
import { USER } from '../../constants/localStorageKeys';
import * as ROUTES from '../../constants/routes';

const LoginForm = (props) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [rememberMe, setRememberMe] = useState(false)
    const [validationError, setValidationError] = useState({ hasError: false, message: '' })
    // event handlers
    const onEmailChangeHandler = e => {
        setValidationError({
            hasError: false,
            message: ''
        })
        setEmail(e.target.value)
    }
    const onPasswordChangeHandler = e => {
        setValidationError({
            hasError: false,
            message: ''
        })
        setPassword(e.target.value)
    }
    const onRememberMeChangeHandler = e => {
        e.persist()
        setRememberMe(e.target.checked)
    }
    const onSubmitHandler = e => {
        e.preventDefault()
        // validate input
        const { error } = userLoginSchema.validate({ email, password })
        if (error) {
            setValidationError({
                hasError: true,
                message: error.message
            })
        } else {
            // check credentials
            if (email === validUser.email && password === validUser.password) {
                // save user to store
                props.onUserLogin(email)
                // update local storage if remember me is clicked
                if (rememberMe)
                    localStorage.setItem(USER, email)
                // redirect to actors
                props.history.push(ROUTES.ACTORS_LIST_VIEW)
            } else {
                setValidationError({
                    hasError: true,
                    message: 'Incorrect credentials. Please try again...'
                })
            }
        }
    }
    // view
    return (
        <form className="form-signin">
            <h1 className="h3 mb-3 font-weight-normal">Sign In</h1>
            <label htmlFor="inputEmail" className="sr-only">Email address</label>
            <input
                type="email"
                id="inputEmail"
                className="form-control"
                placeholder="Email address"
                autoFocus
                value={email}
                onChange={onEmailChangeHandler}
            />
            <label htmlFor="inputPassword" className="sr-only">Password</label>
            <input
                type="password"
                id="inputPassword"
                className="form-control"
                placeholder="Password"
                value={password}
                onChange={onPasswordChangeHandler}
            />
            <Checkbox
                large
                checked={rememberMe}
                label="Remember me"
                onChange={onRememberMeChangeHandler}
            />
            {validationError.hasError && <p className="input-error">{validationError.message}</p>}
            <Button
                active large intent={Intent.PRIMARY}
                type="submit"
                onClick={onSubmitHandler}
                text="Sign in"
            />
            <p className="mt-5 mb-3 text-muted">© 2019-2020</p>
        </form>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        onUserLogin: data => dispatch({ type: actionTypes.USER_LOGIN, data })
    }
}

export default withRouter(connect(null, mapDispatchToProps)(LoginForm));
