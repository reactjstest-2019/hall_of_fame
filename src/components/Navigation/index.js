import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
import { isAuthenticated } from '../../util/index';

import './index.scss';

const Navigation = props => {
    let view = null
    if (isAuthenticated(props.user)) {
        view = <p className="mainNav">
            Click <Link to={ROUTES.ACTORS_LIST_VIEW}>here</Link> to see the Actors!
            </p>
    } else {
        view = <p className="mainNav">Please <Link to={ROUTES.LOGIN}>login</Link>!</p>
    }
    return view
}

const mapStateToProps = state => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Navigation);
