import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Intent } from '@blueprintjs/core';
import { isAuthenticated } from '../../util/index';
import * as actionTypes from '../../store/actions';
import * as ROUTES from '../../constants/routes';
import * as styles from './index.module.scss';

const Header = props => {
    const logOutHandler = e => {
        // remove user from store and localstorage
        props.onUserLogout()
        localStorage.clear()
        // redirect to login
        props.history.push(ROUTES.LOGIN)
    }
    return (
        <header className={styles.header}>
            <h1 className={styles.heading}>Hall of Fame</h1>
            {isAuthenticated(props.user) ?
                <Button
                    large
                    className={styles.btnLogOut}
                    intent={Intent.DANGER}
                    text="Log Out"
                    onClick={logOutHandler}
                />
                : null}
        </header>
    )
}

const mapStateToProps = state => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserLogout: () => dispatch({ type: actionTypes.USER_LOGOUT })
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
