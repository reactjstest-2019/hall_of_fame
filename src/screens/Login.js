import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { LoginForm } from '../components';
import { isAuthenticated } from '../util/index';
import * as ROUTES from '../constants/routes';

const Login = props => isAuthenticated(props.user) ? <Redirect to={ROUTES.HOME} /> : <LoginForm />

const mapStateToProps = state => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Login);
