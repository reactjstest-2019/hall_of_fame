export { default as Home } from './Home';
export { default as Login } from './Login';
export { default as Actors } from './Actors';
export { default as ActorDetails } from './ActorDetails';
