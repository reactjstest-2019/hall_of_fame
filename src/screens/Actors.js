import React, { Component } from 'react';
import axios from 'axios';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { ActorListView } from '../components';
import * as actionTypes from '../store/actions';

export class Actors extends Component {
    constructor(props) {
        super(props);
        // initial state
        this.state = {
            data: [],
            // match current page with react-paginate
            currentPage: this.props.actors.currentPage > 0 ? this.props.actors.currentPage - 1 : 0,
            totalPages: 0,
            limit: 5
        };
    }
    // event handlers
    loadActors = () => {
        // check if current page is in store
        if (this.props.actors.actors[`p${this.state.currentPage}`]) {
            const offset = Math.floor(this.props.actors.totalCount / this.state.limit)
            this.setState({
                data: this.props.actors.actors[`p${this.state.currentPage}`],
                totalPages: offset < 1 ? 1 : offset
            });
        } else {
            // load actors from server and save them to store
            axios.get(`${process.env.REACT_APP_API_SERVER}/actors/${this.state.currentPage}/${this.state.limit}`)
                .then(res => {
                    const offset = Math.floor(res.data.data.count / this.state.limit)
                    this.setState({
                        data: res.data.data.list,
                        totalPages: offset < 1 ? 1 : offset
                    });
                    this.props.onActorsUpdate({
                        page: this.state.currentPage,
                        list: res.data.data.list,
                        totalCount: res.data.data.count
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }
    onPageChangeHandler = data => {
        const selected = data.selected + 1;

        this.setState({ currentPage: selected }, () => {
            this.loadActors();
        });
    }
    render() {
        return (
            <>
                <ActorListView list={this.state.data} />
                <ReactPaginate
                    previousLabel={'< previous'}
                    nextLabel={'next >'}
                    breakLabel={'...'}
                    initialPage={this.state.currentPage}
                    breakClassName={'break-me'}
                    pageCount={this.state.totalPages}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={this.onPageChangeHandler}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                />
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        actors: state.actors
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onActorsUpdate: data => dispatch({ type: actionTypes.ACTORS_UPDATE, data })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Actors);
